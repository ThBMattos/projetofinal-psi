const User = require("../models/User");
const Auth = require("../config/auth");

const create = async (req, res) => {
    try {
      const { password } = req.body;
      const hashAndSalt = Auth.generatePassword(password);
      const salt = hashAndSalt.salt;
      const hash = hashAndSalt.hash;
      const newUserData = {
        name: req.body.name,
        email: req.body.email,
        document: req.body.document,
        gender: req.body.gender,
        hash: hash,
        salt: salt,
      };
      const user = await User.create(newUserData);
      console.log(user);
      return res.status(201).json({mensage:"Usuário criado com sucesso.", user: user });
    } catch (e) {
      return res.status(500).json('Dados não cadastrados!');
    }
  };

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        if (user) {
            return res.status(200).json({ user: user });
        }
        throw new error();
    } catch (e) {
        return res.status(500).json('Usuário não encontrado!');
    }
};

const index = async (req, res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({ users });
    } catch (e) {
        return res.status(500).json('Usuários não encontrados!');
    }
}

const update = async (req, res) => {
    const { id } = req.params;
    try {
        if (req.body.password) {
            const updateUserData = {
                name: req.body.name,
                email: req.body.email,
                document: req.body.document,
                gender: req.body.gender,
            }
            const [updated] = await User.update(updateUserData, { where: { id: id } });
            if (updated) {
                const user = await User.findByPk(id);
                return res.status(200).send(user);
            }
            throw new Error();
        }

        const [updated] = await User.update(req.body, { where: { id: id } });
        if (updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Credencias não encontradas!");
    }
};


const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json('Usuário deletado com sucesso!');
        }
        throw new error();
    } catch (e) {
        return res.status(500).json('Usuário não encontrado!');
    }
}

module.exports = {
    create,
    show,
    index,
    update,
    destroy
}