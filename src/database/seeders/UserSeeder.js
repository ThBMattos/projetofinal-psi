const User = require("../../models/User");
const faker = require("faker-br");

const seedUser = async function () {
    try {
        await User.sync({ force: true });
        const users = [];

        for (let i = 0; i < 10; i++) {
            let user = await User.create({
                name: faker.name.firstName(),
                email: faker.internet.email(),
                document: faker.br.cpf(),
                gender: "Feminino",

                // name: 
                // email:
                // document:
                // gender:
            })
        }

    } catch (error) {
        console.log(error + "!");
    }
}

module.exports = seedUser;