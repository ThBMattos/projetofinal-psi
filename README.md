# Projeto final PSI 22.1 | EJCM
Projeto feito para o processo seletivo interno para o cargo de tech lead feito em Node.js e Express contendo CRUD básica de duas entidades relacionadas entre si e consumindo uma API pública para autenticação. 
 
**Status do Projeto** : Finalizado


![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![Badge](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
 
 
## Tabela de Conteúdo

*Faça um índice com links internos para todos os tópicos seguintes.*

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Configuração](#configuração)
 4. [Uso](#uso)
 6. [Autores](#autores)
 
## Tecnologias utilizadas

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

 - [Node](https://nodejs.org/en/) versão ^16.13.1

## Instalação 

Na raiz do projeto, basta utilizar o comando

``` bash
$ npm install
```

## Configuração

Veremos a seguir os comandos necessários para configurar o projeto.

Primeiramente devemos fazer a cópia do arquivo .env

``` bash
$ cp .env.example .env
```

E então, devemos criar o banco e popula-lo

``` bash
$ npm run migrate
```
``` bash
$ npm run seed
```
 
## Uso

Após realizar os passos de configuração, basta digitar o comando a seguir para rodar a API

``` bash
$ npm run dev
```

## Autores

* Dev Back-end - Thiago Barcellos Mattos  
 

## Última atualização: 17/04/2022