const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define("Product", {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    value: {
        type: DataTypes.STRING,
        allowNull: false
    },
    quantity: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
    },
});

Product.associate = function(models){
    Product.belongsTo(models.User);
}

module.exports = Product;