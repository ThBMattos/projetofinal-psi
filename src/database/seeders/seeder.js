require('../../config/dotenv')();
require('../../config/sequelize');

//const seedModel = require('./Model');
const seedUser = require("../seeders/UserSeeder");
const seedProduct = require("../seeders/ProductSeeder");

(async () => {
  try {
    //await seedModel();
    await seedUser();
    await seedProduct();

  } catch(err) { console.log(err) }
})();
