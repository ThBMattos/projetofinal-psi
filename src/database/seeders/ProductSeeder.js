const Product = require("../../models/Product");
const faker = require("faker-br");

const seedProduct = async function () {
  try {
    await Product.sync({ force: true });
    const products = [];

    for (let i = 0; i < 10; i++) {
      let product = await Product.create({
        name: faker.commerce.productName(),
        value: faker.commerce.price(),
        quantity: faker.random.number(),
        description: faker.lorem.sentences(),
      });
    }
  } catch (error) {
    console.log(error + "!");
  }
};

module.exports = seedProduct;
