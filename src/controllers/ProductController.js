const Product = require("../models/Product");

const create = async (req, res) => {
    try {
        const newProductData = {
            name: req.body.name,
            value: req.body.value,
            quantity: req.body.quantity,
            description: req.body.description,
        };
        const product = await Product.create(newProductData);
        console.log(product);
        return res.status(201).json({ mensage: "Produto criado com sucesso.", product: product });

    } catch (e) {
        return res.status(500).json('Dados não cadastrados!');
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const product = await Product.findByPk(id);
        if (product) {
            return res.status(200).json({ product: product });
        }
        throw new error();
    } catch (e) {
        return res.status(500).json('Produto não encontrado!');
    }
};

const index = async (req, res) => {
    try {
        const products = await Product.findAll();
        return res.status(200).json({ products });
    } catch (e) {
        return res.status(500).json('Produtos não encontrados!');
    }
}

const update = async (req, res) => {
    const { id } = req.params;
    try {
        if (req.body.password) {
            const updateProductData = {
                name: req.body.name,
                email: req.body.email,
                document: req.body.document,
                gender: req.body.gender,
            }
            const [updated] = await Product.update(updateProductData, { where: { id: id } });
            if (updated) {
                const product = await Product.findByPk(id);
                return res.status(200).send(product);
            }
            throw new Error();
        }

        const [updated] = await Product.update(req.body, { where: { id: id } });
        if (updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Produto não encontrado!");
    }
};


const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await Product.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json('Produto deletado com sucesso!');
        }
        throw new error();
    } catch (e) {
        return res.status(500).json('Produto não encontrado!');
    }
}

module.exports = {
    create,
    show,
    index,
    update,
    destroy
}